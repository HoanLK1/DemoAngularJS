import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { ToasterService } from "../service/toaster.service";

@Injectable()
export class AdminGuard implements CanActivate {

    constructor(private _router: Router, private _toast: ToasterService) { }

    public canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        if (typeof window !== "undefined") {
            var token = localStorage.getItem('currAdmin');
            console.log(token);
            if (token == undefined) {                
                this._toast.pushDanger("Login to access Admin page")
                this._router.navigate(["admin/login"]);
                return false;
            }   
            else {               
                return true;
            }    
        }
    }
}