import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import * as $ from 'jquery';
import { AppComponent } from './component/app/app.component';
import { routes, routing } from './app.routes';
import { HomeComponent } from "./component/home/home.component";
import { IndexComponent } from "./component/home/index/index.component";
import { ListflowerComponent } from "./component/home/listflower/listflower.component";
import { NavmenuComponent } from "./component/navmenu/navmenu.component";
import { FooterComponent } from "./component/footer/footer.component";
import { SlideComponent } from "./component/home/slide/slide.component";
import { FeedbackComponent } from "./component/home/feedback/feedback.component";
import { BestsellComponent } from "./component/home/bestsell/bestsell.component";
import { BlogComponent } from "./component/home/blog/blog.component";
import { ServiveComponent } from "./component/home/service/service.component";
import { SubscribeComponent } from "./component/home/subscribe/subscribe.component";
import { ManufactureComponent } from "app/component/home/manufacture/manufacture.component";
import { BacktopComponent } from "app/component/backtop/backtop.component";
import { SocialComponent } from "app/component/home/sticky-social/sticky-social.component";
import { LoginComponent } from "app/component/navmenu/login/login.component";
import { DetailComponent } from "app/component/detail/detail.component";
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { HttpModule } from "@angular/http";
import { ToasterService } from "app/service/toaster.service";
import { ProfileComponent } from "app/component/profile/profile.component";
import { UserService } from "app/service/user.service";
import { ProductService } from "app/service/product.service";
import { NameComponent } from "app/component/home/item-product/item-product.component";
import { LoginAdminComponent } from "app/component/admin/login/login.component";
import { HomeAdminComponent } from "app/component/admin/homeAdmin.component";
import { AdminGuard } from "app/guards/admin.guard";
import { AdminService } from "app/service/admin/user.service";
import { SidebarComponent } from "app/component/admin/side-bar/side-bar.component";
import { HeaderbarComponent } from "app/component/admin/header-bar/header-bar.component";
import { PagetitleComponent } from "app/component/admin/page-title/page-title.component";
import { ProductComponent } from "app/component/admin/product/product.component";
import { ProfileAdminComponent } from "app/component/admin/profile/profile.component";
import { ProductServiceAdmin } from "app/service/admin/product.service";

@NgModule({
  bootstrap: [AppComponent],
  declarations: [
    AppComponent,
    HomeComponent,
    IndexComponent,
    ListflowerComponent,
    NavmenuComponent,
    FooterComponent,
    SlideComponent,
    FeedbackComponent,
    BestsellComponent,    
    BlogComponent,
    ServiveComponent,
    SubscribeComponent,
    ManufactureComponent,
    BacktopComponent,
    SocialComponent,   
    LoginComponent,
    DetailComponent,
    ProfileComponent,
    NameComponent,    
    HomeAdminComponent,
    LoginAdminComponent,
    SidebarComponent,
    HeaderbarComponent,
    ProductComponent,
    PagetitleComponent,
    ProfileAdminComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    routing,
    HttpModule,
  ],
  providers: [
    ToasterService,
    UserService,
    AdminService,
    ProductService,
    AdminGuard,
    ProductServiceAdmin
  ],  
})

export class AppModule{

}