import { RouterModule, Routes, CanActivate  } from "@angular/router";
import { ModuleWithProviders } from "@angular/core";
import { HomeComponent } from "./component/home/home.component";
import { IndexComponent } from "./component/home/index/index.component";
import { ListflowerComponent } from "./component/home/listflower/listflower.component";
import { DetailComponent } from "app/component/detail/detail.component";
import { ProfileComponent } from "app/component/profile/profile.component";
import { LoginAdminComponent } from "app/component/admin/login/login.component";
import { HomeAdminComponent } from "app/component/admin/homeAdmin.component";
import { AdminGuard } from "app/guards/admin.guard";
import { ProductComponent } from "app/component/admin/product/product.component";
import { ProfileAdminComponent } from "app/component/admin/profile/profile.component";

export const routes: Routes = [
    {
        path: '', component: HomeComponent, children: [
            { path: '', component: IndexComponent },
            { path: 'list-flower', component: ListflowerComponent },
            { path: 'detail', component: DetailComponent },
        ],
    },
    {
        path: 'admin', canActivate: [AdminGuard] , component: HomeAdminComponent, children: [
            { path: '', canActivate: [AdminGuard], component: ProductComponent },
            { path: 'profile', canActivate: [AdminGuard], component: ProfileAdminComponent}
        ]
    },
    {
        path: 'profile/:id', component: ProfileComponent
    },
    {
        path: 'admin/login', component: LoginAdminComponent

    },
    { path: '**', redirectTo: '' }
]

export const routing: ModuleWithProviders = RouterModule.forRoot(routes);
