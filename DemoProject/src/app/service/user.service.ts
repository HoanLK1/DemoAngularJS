import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, URLSearchParams } from "@angular/http";
import { Observable } from 'rxjs/observable';
import { environment } from '../../environments/environment';
import { Account } from './user';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class UserService {
    handleError: any;
    baseUrl: string = environment.rootUrl + "Account/";
    headers = new Headers({ 'Content-type': 'application/json' });
    constructor(private http: Http) { };
    getAccount(): Observable<Account[]> {
        return this.http.get(this.baseUrl)
            .map((res: Response) => res.json() as Account[]);
    }
    getInfomation(idUser: number): Observable<any> {
        return this.http.get(this.baseUrl + idUser, { headers: this.headers })
            .catch((er) => {
                return this.handleError;
            })
    }
    signUp(model: any): Observable<Response> {
        var user = new Account();
        user.email = model.email;
        user.password = model.password;
        user.firstName = model.firstName;
        user.lastName = model.lastName;
        return this.http
            .post(this.baseUrl, user, { headers: this.headers })
            // .toPromise()           
            .catch((err) => {
                return this.handleError;
            });
    }
    updateAccount(account: Account): Observable<any> {
        return this.http.put(this.baseUrl + account.id, JSON.stringify(account), { headers: this.headers })
            .map(() => {
                return account;
            })
    }
}
