import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import * as $ from 'jquery';
@Injectable()
export class ToasterService {
    constructor() { }
    async push(message, type) {
        try {
            // await $('body').append(`<div class="modal modal1  right" id="toastModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: block; padding-right: 17px;">
            //         <div class="modal-dialog modal-size modal-bottom-right modal-notify modal-`+ type + `" role="document">
            //             <div class="modal-content">
            //                 <div class="modal-header">
            //                     <p class="heading lead">` + message + ` </p>                                
            //                 </div>
            //                 <div class="modal-footer">
            //                     <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            //                 </div>
            //             </div>
            //         </div>
            //     </div>
            // `);

            // $('#toastModal').modal({backdrop: false});
            // setTimeout(() => {
            //     $('#toastModal').modal('toggle');
            // }, 1000);
            // setTimeout(() => {
            //     $('#toastModal').remove();
            // }, 1500);
            setTimeout(function () {
                $('body').append(`<div class="modal fade right " id="toastModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: block; padding-right: 17px;">
                        <div class="modal-dialog modal-side modal-bottom-right modal-notify modal-`+ type + `" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <p class="heading lead">` + message + ` </p>                                
                                </div>                               
                            </div>
                        </div>
                    </div>
                `);           
            }, 0);
            setTimeout(() => {              
                $('#toastModal').remove();
            }, 2000);                
        } catch (ex) { }
    }
    async pushSuccess(message) {       
        this.push(message, "success");
    }
    async pushWarning(message) {
        this.push(message, "warning");
    }
    async pushInfo(message) {
        this.push(message, "info");
    }
    async pushDanger(message) {
        this.push(message, "danger");
    }
}