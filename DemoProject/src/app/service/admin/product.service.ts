import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/observable';
import { environment } from '../../../environments/environment';
import { Product } from '../../_model/product';
import { Http,Response, Headers } from "@angular/http";
// import 'rxjs/add/operator/map';


@Injectable()
export class ProductServiceAdmin{
    baseUrl: string = environment.rootUrl + "Product/";
    headers = new Headers({ 'Content-type': 'application/json'});
    constructor(private http: Http){ };
    getProduct(): Observable<Product[]>{
        return this.http.get(this.baseUrl,  { headers: this.headers })
            .map((res: Response) => res.json() as Product[]);
    }
}