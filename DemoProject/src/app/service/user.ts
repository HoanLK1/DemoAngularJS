export class Account{
    id: number;
    email: string;
    password: string;
    confirmPassword: string;
    firstName: string;
    lastName: string;
    dateofBirth: number;
    avatarUrl =  "https://image.ibb.co/h2sprv/user.png";
}