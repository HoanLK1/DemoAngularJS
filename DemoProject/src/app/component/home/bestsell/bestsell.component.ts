import { Component, OnInit, Input } from '@angular/core';
import { ProductService } from '../../../service/product.service';
import { Product } from '../../../_model/product';
import * as $ from 'jquery';

@Component({
    selector: 'bestsell',
    templateUrl: './bestsell.component.html',
    styleUrls: ['./bestsell.component.css']
})

export class BestsellComponent {
    activeRow: Product[];
    listOfLists: any[] = [];
    itemProduct: any = [];
    length: number;
    constructor(private _product: ProductService) { }
    ngOnInit(): void {
        this._product.getProduct()
            .subscribe((itemProduct: Product[]) => {
                this.itemProduct = itemProduct
                this.sort(this.itemProduct);               
                console.log(this.itemProduct);
                if (this.itemProduct.length % 2 != 0) { // neu chi so item bi le 1 dong ( dong do ko du 4 item)
                    this.length = Math.floor(this.itemProduct.length / 2) + 1; // thi ta chi 4 lay nguyen + 1
                }
                else {
                    this.length = this.itemProduct.length / 2;
                }
                let index = 0;
                for (let k = 0; k < this.length; k++) {
                    let itemChild = []; // mang tam de nhan 4 phan tu moi lan duyet
                    for (let j = 0; j < 2; j++) { // lay 2 phan tu moi lan trong day product
                        if (index < this.itemProduct.length) { // neu vi tri dang xet chua wa phan tu cuoi
                            itemChild[j] = this.itemProduct[index]; // gan lan luot 4 dua zo item child
                            index++;
                        }
                        if (k == 0) { // new la thuoc day product dau tien 
                            this.activeRow = itemChild; // thi gan vao active row
                        } else { // new khong thuoc day product dau tien
                            this.listOfLists[k - 1] = itemChild; // nhet moi day product vo mang cha
                        }
                    }

                }
                console.log(this.activeRow);
                console.log(this.listOfLists);
        });
    }
    sort(arr: any[]) {
        var length = this.itemProduct.length;
        for (var i = 0; i < length; i++) { //Number of passes
            for (var j = 0; j < (length - i - 1); j++) { //Notice that j < (length - i)
                //Compare the adjacent positions
                if (this.itemProduct[j].Sale < this.itemProduct[j + 1].Sale) {
                    //Swap the numbers                   
                    let tmp = this.itemProduct[j];  //Temporary variable to hold the current number
                    this.itemProduct[j] = this.itemProduct[j + 1]; //Replace current number with adjacent number
                    this.itemProduct[j + 1] = tmp; //Replace adjacent number with current number                    
                }
            }
        }
    }
}