import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { Account } from '../../service/user';
import { ToasterService } from '../../service/toaster.service';
import { RouterLink } from '@angular/router';
@Component({
    selector: 'navmenu',
    templateUrl: './navmenu.component.html',
    styleUrls: ['./navmenu.component.css'],
})

export class NavmenuComponent implements OnInit {
    user: Account;
    name: string = '';
    isLogin: boolean = false;
    firstName: string = '';
    userId: number;
    avatarUrl: string ='';
    constructor(private _toaster: ToasterService) { }
    ngOnInit(): void {
        if (typeof window !== "undefined") {
            $(document).ready(function () {
                $(window).scroll(function () {
                    if ($(".navbar").offset().top > 100) {
                        $(".navbar-fixed-top").addClass("top-nav-collapse");
                    }
                    else {
                        $(".navbar-fixed-top").removeClass("top-nav-collapse");
                    }
                });
                $(".page-scroll").on('click', function (event) {
                    if (this.hash !== "") {
                        event.preventDefault();
                        var hash = this.hash;
                        $('html, body').animate({
                            scrollTop: $(hash).offset().top-100
                        }, 800, function () {
                            window.location.hash = hash;
                        });
                    }
                });
                //hamburger menu
                $('.hamburger-menu').on('click', function () {
                    $('.bar').toggleClass('animate');
                    $('.main-menu-mobile').toggleClass('open-menu-mobile');
                });
                $('.button-search').on('click', function () {
                    $(this).toggleClass('active');
                    $('.nav-search').toggleClass('hide');
                });
                //hide box seach when click outside
                $('body').on('click', function (event) {
                    if ($('.button-search').has(event.target).length === 0 && !$('.button-search').is(event.target) && $('.nav-search').has(event.target).length === 0 && !$('.nav-search').is(event.target)) {
                        if ($('.nav-search').hasClass('hide') === false) {
                            $('.nav-search').toggleClass('hide');
                            $('.button-search').toggleClass('active');
                        }
                    }
                });
            });
        }
        this.user = JSON.parse(localStorage.getItem("currentAccount"));        
        if (this.user == null) {
            this.isLogin = false;
        } else {
            this.isLogin = true;
            this.name = this.user.firstName + " " + this.user.lastName;
            this.userId = this.user.id;
            this.avatarUrl = this.user.avatarUrl;           
        }
    }
    logOut() {
        this.isLogin = false;
        localStorage.clear();
        this.name = "";
        this._toaster.pushSuccess("Logout Success");
    }
    changeName(name) {
        this.isLogin = true;
        this.user = JSON.parse(localStorage.getItem("currentAccount"));
        this.name = this.user.firstName + " " + this.user.lastName;
        this.name = name;
        this.userId = this.user.id;
        this.avatarUrl = this.user.avatarUrl;        
    }
    
}
