import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModel, FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { UserService } from '../../../service/user.service';
import { Account } from '../../../service/user';
import { PasswordValidation } from './password-validation';
import * as $ from 'jquery';
import 'rxjs/add/operator/catch';
import { ToasterService } from '../../../service/toaster.service';
@Component({
    selector: 'login',
    templateUrl: './login-2.component.html',
    styleUrls: ['./login.component.css'],
    providers: [UserService],
})
export class LoginComponent implements OnInit {
    isErrors: boolean = false;
    account: Account[];
    model: Account = new Account();
    email: string = "";
    password: string = "";
    isLogin: boolean = true;
    fullName: string;
    @Output() getName = new EventEmitter();
    form: FormGroup;
    constructor(private userService: UserService, private formbuilder: FormBuilder, private _toast: ToasterService) {
        this.form = formbuilder.group({
            email: this.formbuilder.control('', [Validators.required, Validators.email]),
            firstName: ['', Validators.required],
            lastName: ['', Validators.required],
            password: this.formbuilder.control('', [Validators.required, Validators.minLength(6)]),
            confirmPassword: ['', Validators.required]
        }, {
                validator: PasswordValidation.MatchPassword
            })
    };
    getAccount(): void {
        this.userService.getAccount().subscribe(accounts => {
            let result = accounts.some(account => {
                if (account.email === this.email && account.password === this.password) {
                    localStorage.setItem('currentAccount', JSON.stringify(account));
                    this.fullName = account.firstName + " " + account.lastName;
                    this.getMyName(this.fullName);
                }                    
                return account.email === this.email && account.password === this.password;
            })
            if (result == true) {
                this._toast.pushSuccess("Login is succsess!");
                setTimeout(() => {
                    $('#loginModal').css("display","none");
                }, 500);
            }
            else {
                this._toast.pushDanger("Login is fail!");
            }
        });
    };
    add(): void {    
        this.userService.signUp(this.model)
            .catch(err => {
                this.isErrors = true;
                this._toast.pushDanger("Register is fail!");
                this.form.reset();
                return err;
            })
            .subscribe(model => {
                this.isLogin = true;                
                this.form.reset();                      
                this.isErrors = false;
                this._toast.pushSuccess("Register is succsess!");                
            });
    }
    ngOnInit(): void {
        this.isLogin = true;
    }
    loginView() {
        this.isLogin = true;
        this.form.reset();
    }
    signUpView() {
        this.isLogin = false;
        this.form.reset();
    }
    getMyName(name) {       
        this.getName.emit(name);
    }

}