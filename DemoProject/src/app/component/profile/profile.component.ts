import { Component, OnInit, Input, Output, EventEmitter, ElementRef, ViewChild } from '@angular/core';
import { ToasterService } from '../../service/toaster.service';
import { UserService } from '../../service/user.service';
import { NgModel, FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { Account } from '../../service/user';
import * as $ from 'jquery';
import { Router } from '@angular/router';
@Component({
    selector: 'profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
    constructor(private _toaster: ToasterService, private _user: UserService, private router: Router) { }
    isLogin: boolean = true;
    user: any;
    name: string;
    idUser: number;
    newDate: Date;
    bindDate: String;
    dateFormat = new Date();
    link: string;    
    @Input() value: Date;
    @ViewChild("inputAvatar") fileInput: ElementRef;
    ngOnInit(): void {
        this.user = JSON.parse(localStorage.getItem("currentAccount"));
        if (this.user == null) {
            this.isLogin = false;
        } else {
            this.isLogin = true;
            this.name = this.user.firstName + " " + this.user.lastName;
            this.idUser = this.user.id;
        }
        this._user.getInfomation(this.idUser).subscribe(user => {
            let _curUser = user.json() as Account;
            this.newDate = new Date(_curUser.dateofBirth);
            this.bindDate = this.parse(this.newDate);            
            return user;
        });
    }
    parse(date: Date): string {
        let y = date.getFullYear();
        let m = date.getMonth() + 1;
        let d = date.getDate();
        let month = m >= 10 ? m : "0" + m;
        let day = d >= 10 ? d : "0" + d;        
        let res = y + "-" + month + "-" + day;
        return res;
    }

    logOut() {
        this.isLogin = false;
        localStorage.clear();
        this.name = "";
        this._toaster.pushSuccess("Logout Success");        
        this.router.navigate(['/']);
    }
    
    thayDoiThoiGian(date) {
        this.user.dateofBirth = date;        
    }
    updateProfile() {        
        // $(".btn-info").prop("disabled", false);
        $(".card-block-before").css("display","none");
    }
    saveChange() {        
        this._user.updateAccount(this.user).subscribe(result => {            
            localStorage.setItem('currentAccount', JSON.stringify(result));
            this.name = result.firstName + " " + result.lastName;            
            if (result != null) {
                this._toaster.pushSuccess("Edit is succes!");
            }
            else {
                this._toaster.pushDanger("Edit is fail!");
            }
        })
        $(".btn-info").prop("disabled", true);
        $(".card-block-before").css("display","block");
    }
    backHome() {      
        this.router.navigate(['/']);
    }
}