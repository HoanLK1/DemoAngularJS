import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
@Component({
    selector: 'homeAdmin',
    templateUrl: './home.component.html',
})
export class HomeAdminComponent implements OnInit {
    ngOnInit(): void {          
          if (typeof window !== "undefined") {           
                $(document).ready(() => {     
                // Material Select Initialization
                $(document).ready(function() {
                    $('.mdb-select').material_select();
                });

                // Sidenav Initialization
                $(".button-collapse").sideNav();

                // Tooltips Initialization
                $(function() {
                    $('[data-toggle="tooltip"]').tooltip()
                    $('#toggle').tooltip('show')
                })
                // $("#home-sidebar").addClass('active');                      
            })
        }      
          
    }    
}


