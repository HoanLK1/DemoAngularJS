import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { ProductServiceAdmin } from '../../../service/admin/product.service';
import { Product } from '../../../_model/product';

@Component({
    selector: 'product',
    templateUrl: './product.component.html',
    styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
    itemproduct: Product[];
    constructor(private _prdservice: ProductServiceAdmin) { }

    ngOnInit() { 
        this._prdservice.getProduct().subscribe(itemproduct =>{
            console.log(itemproduct);
            this.itemproduct = itemproduct;
            console.log(this.itemproduct);
        })
    }

}