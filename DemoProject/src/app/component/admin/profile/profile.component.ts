import { Component, OnInit } from '@angular/core';
import { AdminService } from "../../../service/admin/user.service";
import { ToasterService } from '../../../service/toaster.service';
import { Admin } from "../../../_model/admin";
import * as $ from 'jquery';
@Component({
    selector: 'profile-admin',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.css']
})
export class ProfileAdminComponent implements OnInit {
    clickEnabel: boolean = false;
    constructor(private _profile: AdminService, private toaster: ToasterService) { }
    accounts: any;
    idUser: number;
    ngOnInit() {      
        this.accounts = JSON.parse(localStorage.getItem("currAdmin"));
        this.idUser = this.accounts.id;          
        this._profile.getInfomation(this.idUser).subscribe(accounts => {            
            this.accounts = accounts;     
            console.log(this.accounts);        
        });    
    }
    enabel(){          
        console.log(this.accounts);  
        return this.clickEnabel =  true;
    }
    cancel(){
        return this.clickEnabel = false;
    }
    save(){
        this._profile.updateAccount(this.accounts).subscribe(result => {
            localStorage.setItem('currAdmin', JSON.stringify(result));
            if(result != null){
                this.toaster.pushSuccess("Update profile is succes!");
            }
            else{
                this.toaster.pushDanger("Update profile is fail!");
            }
        })
    }
}