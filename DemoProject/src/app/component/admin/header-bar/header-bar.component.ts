import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
    selector: 'header-bar',
    templateUrl: 'header-bar.component.html',
    styleUrls: ['./header-bar.component.css']
    
})
export class HeaderbarComponent implements OnInit {

    constructor(private _router: Router) { }

    ngOnInit() { 

    }
    logOut() {       
        this._router.navigate(["admin/login"]);
        localStorage.removeItem("currAdmin");        
    }
}