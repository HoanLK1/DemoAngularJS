import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModel, FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { NgForm } from "@angular/forms";
import { ToasterService } from "../../../service/toaster.service";
import { AdminService } from "../../../service/admin/user.service";
import { Admin } from "../../../_model/admin";
import * as $ from "jquery";
@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css'],
})
export class LoginAdminComponent implements OnInit {
    admin: Admin[];
    model: Admin = new Admin();
    Username: string ="";
    Password: string= "";
    constructor(private _toast: ToasterService, private _admin: AdminService, private _router: Router) { }
    getAccount(): void {
        this._admin.getAccount().subscribe(accounts => {
            // console.log(accounts);
            let result = accounts.some(account => {                        
                if (account.Username === this.Username && account.Password === this.Password)
                    localStorage.setItem('currAdmin', JSON.stringify(account));
                return account.Username === this.Username && account.Password === this.Password;
            })
            if (result == true) {
                this._toast.pushSuccess("Login is succsess!");
                setTimeout(() => {
                    $('#loginModal').css("display","none");
                }, 500);
                this._router.navigate(["admin"]);
            }
            else {
                this._toast.pushDanger("Login is fail!");
            }
        });
    };
    ngOnInit() { 
        
    }
   
}

