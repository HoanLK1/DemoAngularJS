$(document).ready(function () {
    $('.manufacture-wrapper').slick({
        autoplay: false,
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        arrows: false,
    });
    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $('.scrollup').fadeIn();
        } else {
            $('.scrollup').fadeOut();
        }
    });

    $('.scrollup').click(function () {
        $("html, body").animate({
            scrollTop: 0
        }, 600);
        return false;
    });
});


$(document).ready(function(){  
  $("page-scroll").on('click', function(event) {
    if (this.hash !== "") {     
      event.preventDefault();
      var hash = this.hash;
      $('html, body').animate({
        scrollTop: $(hash).offset().top-100
      }, 800, function(){
        window.location.hash = hash;
      });
    } 
  });  
//   $('.datepicker').pickadate();
});

